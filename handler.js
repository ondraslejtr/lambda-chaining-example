"use strict";

const AWS = require("aws-sdk");



// The action lambda
module.exports.print_strings = (event, context, callback) => {  
  const response = {
    statusCode: 200,
    body: JSON.stringify({
      message: `${event} - from the other function`
    })
  };
  callback(null, response);
};

// The cron Lambda
module.exports.cron_launcher = (event, context, callback) => {  
  const fakeDBResults = [
    "Good morning.",
    "How are you?",
    "May I pet your dog?",
    "Oh, that's nice"
  ];

  fakeDBResults.forEach(message_string => {

    const lambda = new AWS.Lambda({  
      region: "eu-central-1"
    });

    const params = {
      FunctionName: "chaining-dev-print_strings", // Has to match name of function running on AWS
      // There is a set format for how function names are format: <service-name>-<stage:dev,prod..>-function_name
      InvocationType: "RequestResponse",
      Payload: JSON.stringify(message_string)
    };

    lambda.invoke(params, function(error, data) {
      if (error) {
        console.error(JSON.stringify(error));
        callback(error);
      } else if (data) {
        console.log(data);
        callback(null, data);
      }
    });
  });
};